import sys
import json
from collections import OrderedDict

from HTTPWrapper import HTTPWrapper
from ProcessText import ProcessText

class Crawler():

	def __init__(self, url, k):
		self.url = url
		self.k = k

	def display(self):
		text_dict = HTTPWrapper(self.url).get_text()
		uni_wf_dict, bi_wf_dict, tri_wf_dict = ProcessText(text_dict).process_text()

		uni_wf_dict = OrderedDict(sorted(uni_wf_dict.items(), key=lambda x: x[1], reverse=True))
		bi_wf_dict = OrderedDict(sorted(bi_wf_dict.items(), key=lambda x: x[1], reverse=True))
		tri_wf_dict = OrderedDict(sorted(tri_wf_dict.items(), key=lambda x: x[1], reverse=True))

		print "Top entries of unigram"
		self.display_top_k_entries(uni_wf_dict)

		print "Top entries of bigram"
		self.display_top_k_entries(bi_wf_dict)

		print "Top entries of trigram"
		self.display_top_k_entries(tri_wf_dict)

		with open("output1.json", "w") as f:
			f.write(json.dumps(uni_wf_dict))
		with open("output2.json", "w") as f:
			f.write(json.dumps(bi_wf_dict))
		with open("output3.json", "w") as f:
			f.write(json.dumps(tri_wf_dict))

	'''
	Function to display top k entries in a dictionary
	'''
	def display_top_k_entries(self, count_dict):
		num_entries = self.k
		for entry in count_dict:
			print entry
			num_entries -= 1
			if num_entries == 0:
				print ""
				return

if __name__== "__main__":
	# Get arguments from command line
	args = sys.argv[1:]
	# Check for the validity of arguments
	if len(args) != 1:
		print "Wrong input format"
		print "Usage: python crawler.py <url>"
	url = args[0]
	k = 20
	Crawler(url, 20).display()
