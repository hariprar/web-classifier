import string
import nltk
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

class ProcessText():
	def __init__(self, text_dict):
		self.debug = False
		self.text_dict = text_dict
		self.weights = {
			"title": 20,
			"meta_desc": 15,
			"header": 10,
			"link_string": 5,
			"normal": 1
		}

	'''
	Function to get stop words
	Returns a list of stop words
	'''
	def get_stop_words(self, ):
		stop =  stopwords.words("english")
		stop.append("none")
		return stop

	'''
	Function to remove the stopwords
	Returns the filtered text
	'''
	def remove_stop_words(self, text):
		stop =  self.get_stop_words()
		text = ' '.join([word for word in text.split() if word not in stop])
		return text

	'''
	Function to remove punctions. Uses standard string punctuations of python
	Returns text with punctuations removed
	'''
	def remove_punctuations(self, text):
		for c in string.punctuation:
			# Checking for '--' instead of '-' as there could be valid words connected
			# with '-'
			if c == '-':
				c = "--"
			text = text.replace(c, "")
		return text

	# Source https://stackoverflow.com/questions/25534214/nltk-wordnet-lemmatizer-shouldnt-it-lemmatize-all-inflections-of-a-word
	def is_noun(self, tag):
	    return tag in ['NN', 'NNS', 'NNP', 'NNPS']

	def is_verb(self, tag):
	    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']

	def is_adverb(self, tag):
	    return tag in ['RB', 'RBR', 'RBS']

	def is_adjective(self, tag):
	    return tag in ['JJ', 'JJR', 'JJS']

	def penn_to_wn(self, tag):
	    if self.is_adjective(tag):
	        return wn.ADJ
	    elif self.is_noun(tag):
	        return wn.NOUN
	    elif self.is_adverb(tag):
	        return wn.ADV
	    elif self.is_verb(tag):
	        return wn.VERB
	    return None

	'''
	Function to tokenize received text
	Returns unigrams, bigrams, trigrams tokens
	'''
	def tokenize(self, text):
		tokens = nltk.word_tokenize(text)
		tokens = nltk.pos_tag(tokens)
		# considered stemming but lemmatizer seems more appropriate in this case
		lemmas = []
		bi_lemmas = []
		tri_lemmas = []
		wnl = WordNetLemmatizer()
		for token in tokens:
			# Skip the lemma if there are problems with lemmatization
			try:
				lemma = wnl.lemmatize(token[0], self.penn_to_wn(token[1]))
			except:
				if self.debug:
					print "Error lemmatizing"
				continue
			lemmas.append(lemma)

		bi_tokens = []
		tri_tokens = []

		#Finding all bigrams
		for token in nltk.bigrams(lemmas):
			# Neglecting bigrams with same words
			if token[0] == token[1]:
				continue
			bi_tokens.append(" ".join(token))

		# Finding all trigrams
		for token in nltk.trigrams(lemmas):
			# Neglecting trigrams with same words
			if token[0] == token[1] or token[1] == token[2]:
				continue
			tri_tokens.append(" ".join(token))

		return lemmas, bi_tokens, tri_tokens

	'''
	Creating a dictionary to hold frequency of tokens found
	Returns a word frequency dictionary
	'''
	def get_total_word_count(self, words_count_dict, tokens, weight):
		for token in tokens:
			if token in words_count_dict:
				words_count_dict[token] += 1 * weight
			else:
				words_count_dict[token] = 1 * weight
		return words_count_dict

	'''
	Function to process text obtained by parsing the html page
	Returns unigram, bigram and trigram token frequency dictionary
	'''
	def process_text(self):
		uni_words_count_dict = {}
		bi_words_count_dict = {}
		tri_words_count_dict = {}

		for key in self.text_dict:
			text = self.text_dict[key]
			text = text.encode('utf-8', errors='ignore')
			text = unicode(text.lower(), errors='ignore')

			text = self.remove_punctuations(text)
			text = self.remove_stop_words(text)

			uni_tokens, bi_tokens, tri_tokens = self.tokenize(text)

			uni_words_count_dict = self.get_total_word_count(uni_words_count_dict, uni_tokens, self.weights[key])
			bi_words_count_dict = self.get_total_word_count(bi_words_count_dict, bi_tokens, self.weights[key])
			tri_words_count_dict = self.get_total_word_count(tri_words_count_dict, tri_tokens, self.weights[key])

		return uni_words_count_dict, bi_words_count_dict, tri_words_count_dict
