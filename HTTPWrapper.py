import urllib2
from urllib2 import HTTPError, URLError

from bs4 import BeautifulSoup

class HTTPWrapper():

	def __init__(self, url):
		self.url = url
		self.response = ""
		self.debug = False

	'''
	Function to fetch HTML response from the given URL
	Returns the HTML response from the server
	'''
	def fetch_page(self):

		req = urllib2.Request(self.url, headers={ 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' })

		# Future work: Retry with the request fails to fetch response
		try:
		    response = urllib2.urlopen(req)
		except HTTPError as e:
		    print 'The server couldn\'t fulfill the request.'
		    print 'Error code: ', e.code
		except URLError as e:
		    print 'We failed to reach a server.'
		    print 'Reason: ', e.reason
		else:
			self.response = response.read()

	'''
	Function to parse the response HTML and construct text
	Returns text dictionary containing tag, text pairs
	'''
	def get_text(self):
		self.fetch_page()
		soup = BeautifulSoup(self.response, 'html.parser')
		ignore_tags_list = ["script", "style", "input", "img"]
		special_tags_list = ["meta", "a", "title", "h1", "h2", "h3", "h4", \
							"h5", "h6", "b", "i", "u"]

		for tag in soup(ignore_tags_list):
			tag.extract()

		text_dict = {}
		#TODO: Return a dictionary of text with keys as title, url, header, normal
		text_dict["title"] = soup.title.get_text()
		text_dict["meta_desc"] = ""
		text_dict["header"] = ""
		text_dict["link_string"] = ""

		# for meta in soup.findAll("meta"):
		# 	text_dict["meta"] += " "+ meta.get("content", " ").lower()
		# 	# print meta.get("content", " ").lower()

		for meta in soup.findAll("meta"):
			metaname = meta.get('name', '').lower()
			metaprop = meta.get('property', '').lower()
			if 'description' == metaname or metaprop.find("description")>0:
				text_dict["meta_desc"] += meta['content'].strip() + ". "
			if 'title' == metaname or metaprop.find("title")>0:
				title = meta['content'].strip()

		for header in soup.find_all(["h1", "h2", "h3", "h4", "b", "i", "u"]):
			try:
				text_dict["header"] += str(header.string) + ". "
			except:
				if self.debug:
					print "header.string parse error"
		text_dict["header"] += ". "

		for link in soup.find_all("a"):
			try:
				text_dict["link_string"] += str(link.string) + ". "
			except:
				if self.debug:
					print "link.string parse error"
			text_dict["link_string"] += ". "

		for tag in soup(special_tags_list):
			tag.extract()

		text_dict["normal"] = soup.get_text(strip=True)
		return text_dict
